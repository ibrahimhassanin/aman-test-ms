package com.isoft.demo.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.isoft.demo.web.rest.TestUtil;

public class PersonalDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonalDetails.class);
        PersonalDetails personalDetails1 = new PersonalDetails();
        personalDetails1.setId(1L);
        PersonalDetails personalDetails2 = new PersonalDetails();
        personalDetails2.setId(personalDetails1.getId());
        assertThat(personalDetails1).isEqualTo(personalDetails2);
        personalDetails2.setId(2L);
        assertThat(personalDetails1).isNotEqualTo(personalDetails2);
        personalDetails1.setId(null);
        assertThat(personalDetails1).isNotEqualTo(personalDetails2);
    }
}
