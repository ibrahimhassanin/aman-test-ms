package com.isoft.demo.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class PersonalDetailsMapperTest {

    private PersonalDetailsMapper personalDetailsMapper;

    @BeforeEach
    public void setUp() {
        personalDetailsMapper = new PersonalDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(personalDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(personalDetailsMapper.fromId(null)).isNull();
    }
}
