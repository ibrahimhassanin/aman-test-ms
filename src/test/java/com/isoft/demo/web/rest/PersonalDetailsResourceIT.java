package com.isoft.demo.web.rest;

import com.isoft.demo.DemoApp;
import com.isoft.demo.config.SecurityBeanOverrideConfiguration;
import com.isoft.demo.domain.PersonalDetails;
import com.isoft.demo.repository.PersonalDetailsRepository;
import com.isoft.demo.service.PersonalDetailsService;
import com.isoft.demo.service.dto.PersonalDetailsDTO;
import com.isoft.demo.service.mapper.PersonalDetailsMapper;
import com.isoft.demo.web.rest.errors.ExceptionTranslator;
import com.isoft.demo.service.dto.PersonalDetailsCriteria;
import com.isoft.demo.service.PersonalDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.isoft.demo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PersonalDetailsResource} REST controller.
 */
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, DemoApp.class})
public class PersonalDetailsResourceIT {

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NO = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_NATIONAL_ID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTH_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTH_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS = "BBBBBBBBBB";

    @Autowired
    private PersonalDetailsRepository personalDetailsRepository;

    @Autowired
    private PersonalDetailsMapper personalDetailsMapper;

    @Autowired
    private PersonalDetailsService personalDetailsService;

    @Autowired
    private PersonalDetailsQueryService personalDetailsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPersonalDetailsMockMvc;

    private PersonalDetails personalDetails;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PersonalDetailsResource personalDetailsResource = new PersonalDetailsResource(personalDetailsService, personalDetailsQueryService);
        this.restPersonalDetailsMockMvc = MockMvcBuilders.standaloneSetup(personalDetailsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalDetails createEntity(EntityManager em) {
        PersonalDetails personalDetails = new PersonalDetails()
            .fullName(DEFAULT_FULL_NAME)
            .mobileNo(DEFAULT_MOBILE_NO)
            .nationalId(DEFAULT_NATIONAL_ID)
            .birthDate(DEFAULT_BIRTH_DATE)
            .email(DEFAULT_EMAIL)
            .adress(DEFAULT_ADRESS);
        return personalDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalDetails createUpdatedEntity(EntityManager em) {
        PersonalDetails personalDetails = new PersonalDetails()
            .fullName(UPDATED_FULL_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .nationalId(UPDATED_NATIONAL_ID)
            .birthDate(UPDATED_BIRTH_DATE)
            .email(UPDATED_EMAIL)
            .adress(UPDATED_ADRESS);
        return personalDetails;
    }

    @BeforeEach
    public void initTest() {
        personalDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonalDetails() throws Exception {
        int databaseSizeBeforeCreate = personalDetailsRepository.findAll().size();

        // Create the PersonalDetails
        PersonalDetailsDTO personalDetailsDTO = personalDetailsMapper.toDto(personalDetails);
        restPersonalDetailsMockMvc.perform(post("/api/personal-details")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personalDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the PersonalDetails in the database
        List<PersonalDetails> personalDetailsList = personalDetailsRepository.findAll();
        assertThat(personalDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        PersonalDetails testPersonalDetails = personalDetailsList.get(personalDetailsList.size() - 1);
        assertThat(testPersonalDetails.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testPersonalDetails.getMobileNo()).isEqualTo(DEFAULT_MOBILE_NO);
        assertThat(testPersonalDetails.getNationalId()).isEqualTo(DEFAULT_NATIONAL_ID);
        assertThat(testPersonalDetails.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
        assertThat(testPersonalDetails.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPersonalDetails.getAdress()).isEqualTo(DEFAULT_ADRESS);
    }

    @Test
    @Transactional
    public void createPersonalDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personalDetailsRepository.findAll().size();

        // Create the PersonalDetails with an existing ID
        personalDetails.setId(1L);
        PersonalDetailsDTO personalDetailsDTO = personalDetailsMapper.toDto(personalDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonalDetailsMockMvc.perform(post("/api/personal-details")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personalDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonalDetails in the database
        List<PersonalDetails> personalDetailsList = personalDetailsRepository.findAll();
        assertThat(personalDetailsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPersonalDetails() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList
        restPersonalDetailsMockMvc.perform(get("/api/personal-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personalDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].nationalId").value(hasItem(DEFAULT_NATIONAL_ID)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS)));
    }
    
    @Test
    @Transactional
    public void getPersonalDetails() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get the personalDetails
        restPersonalDetailsMockMvc.perform(get("/api/personal-details/{id}", personalDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personalDetails.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME))
            .andExpect(jsonPath("$.mobileNo").value(DEFAULT_MOBILE_NO))
            .andExpect(jsonPath("$.nationalId").value(DEFAULT_NATIONAL_ID))
            .andExpect(jsonPath("$.birthDate").value(DEFAULT_BIRTH_DATE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS));
    }


    @Test
    @Transactional
    public void getPersonalDetailsByIdFiltering() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        Long id = personalDetails.getId();

        defaultPersonalDetailsShouldBeFound("id.equals=" + id);
        defaultPersonalDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultPersonalDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPersonalDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultPersonalDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPersonalDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName equals to DEFAULT_FULL_NAME
        defaultPersonalDetailsShouldBeFound("fullName.equals=" + DEFAULT_FULL_NAME);

        // Get all the personalDetailsList where fullName equals to UPDATED_FULL_NAME
        defaultPersonalDetailsShouldNotBeFound("fullName.equals=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName not equals to DEFAULT_FULL_NAME
        defaultPersonalDetailsShouldNotBeFound("fullName.notEquals=" + DEFAULT_FULL_NAME);

        // Get all the personalDetailsList where fullName not equals to UPDATED_FULL_NAME
        defaultPersonalDetailsShouldBeFound("fullName.notEquals=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName in DEFAULT_FULL_NAME or UPDATED_FULL_NAME
        defaultPersonalDetailsShouldBeFound("fullName.in=" + DEFAULT_FULL_NAME + "," + UPDATED_FULL_NAME);

        // Get all the personalDetailsList where fullName equals to UPDATED_FULL_NAME
        defaultPersonalDetailsShouldNotBeFound("fullName.in=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName is not null
        defaultPersonalDetailsShouldBeFound("fullName.specified=true");

        // Get all the personalDetailsList where fullName is null
        defaultPersonalDetailsShouldNotBeFound("fullName.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName contains DEFAULT_FULL_NAME
        defaultPersonalDetailsShouldBeFound("fullName.contains=" + DEFAULT_FULL_NAME);

        // Get all the personalDetailsList where fullName contains UPDATED_FULL_NAME
        defaultPersonalDetailsShouldNotBeFound("fullName.contains=" + UPDATED_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByFullNameNotContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where fullName does not contain DEFAULT_FULL_NAME
        defaultPersonalDetailsShouldNotBeFound("fullName.doesNotContain=" + DEFAULT_FULL_NAME);

        // Get all the personalDetailsList where fullName does not contain UPDATED_FULL_NAME
        defaultPersonalDetailsShouldBeFound("fullName.doesNotContain=" + UPDATED_FULL_NAME);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo equals to DEFAULT_MOBILE_NO
        defaultPersonalDetailsShouldBeFound("mobileNo.equals=" + DEFAULT_MOBILE_NO);

        // Get all the personalDetailsList where mobileNo equals to UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldNotBeFound("mobileNo.equals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo not equals to DEFAULT_MOBILE_NO
        defaultPersonalDetailsShouldNotBeFound("mobileNo.notEquals=" + DEFAULT_MOBILE_NO);

        // Get all the personalDetailsList where mobileNo not equals to UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldBeFound("mobileNo.notEquals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo in DEFAULT_MOBILE_NO or UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldBeFound("mobileNo.in=" + DEFAULT_MOBILE_NO + "," + UPDATED_MOBILE_NO);

        // Get all the personalDetailsList where mobileNo equals to UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldNotBeFound("mobileNo.in=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo is not null
        defaultPersonalDetailsShouldBeFound("mobileNo.specified=true");

        // Get all the personalDetailsList where mobileNo is null
        defaultPersonalDetailsShouldNotBeFound("mobileNo.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo contains DEFAULT_MOBILE_NO
        defaultPersonalDetailsShouldBeFound("mobileNo.contains=" + DEFAULT_MOBILE_NO);

        // Get all the personalDetailsList where mobileNo contains UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldNotBeFound("mobileNo.contains=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByMobileNoNotContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where mobileNo does not contain DEFAULT_MOBILE_NO
        defaultPersonalDetailsShouldNotBeFound("mobileNo.doesNotContain=" + DEFAULT_MOBILE_NO);

        // Get all the personalDetailsList where mobileNo does not contain UPDATED_MOBILE_NO
        defaultPersonalDetailsShouldBeFound("mobileNo.doesNotContain=" + UPDATED_MOBILE_NO);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId equals to DEFAULT_NATIONAL_ID
        defaultPersonalDetailsShouldBeFound("nationalId.equals=" + DEFAULT_NATIONAL_ID);

        // Get all the personalDetailsList where nationalId equals to UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldNotBeFound("nationalId.equals=" + UPDATED_NATIONAL_ID);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId not equals to DEFAULT_NATIONAL_ID
        defaultPersonalDetailsShouldNotBeFound("nationalId.notEquals=" + DEFAULT_NATIONAL_ID);

        // Get all the personalDetailsList where nationalId not equals to UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldBeFound("nationalId.notEquals=" + UPDATED_NATIONAL_ID);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId in DEFAULT_NATIONAL_ID or UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldBeFound("nationalId.in=" + DEFAULT_NATIONAL_ID + "," + UPDATED_NATIONAL_ID);

        // Get all the personalDetailsList where nationalId equals to UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldNotBeFound("nationalId.in=" + UPDATED_NATIONAL_ID);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId is not null
        defaultPersonalDetailsShouldBeFound("nationalId.specified=true");

        // Get all the personalDetailsList where nationalId is null
        defaultPersonalDetailsShouldNotBeFound("nationalId.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId contains DEFAULT_NATIONAL_ID
        defaultPersonalDetailsShouldBeFound("nationalId.contains=" + DEFAULT_NATIONAL_ID);

        // Get all the personalDetailsList where nationalId contains UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldNotBeFound("nationalId.contains=" + UPDATED_NATIONAL_ID);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByNationalIdNotContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where nationalId does not contain DEFAULT_NATIONAL_ID
        defaultPersonalDetailsShouldNotBeFound("nationalId.doesNotContain=" + DEFAULT_NATIONAL_ID);

        // Get all the personalDetailsList where nationalId does not contain UPDATED_NATIONAL_ID
        defaultPersonalDetailsShouldBeFound("nationalId.doesNotContain=" + UPDATED_NATIONAL_ID);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate equals to DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.equals=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate equals to UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.equals=" + UPDATED_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate not equals to DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.notEquals=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate not equals to UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.notEquals=" + UPDATED_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate in DEFAULT_BIRTH_DATE or UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.in=" + DEFAULT_BIRTH_DATE + "," + UPDATED_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate equals to UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.in=" + UPDATED_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate is not null
        defaultPersonalDetailsShouldBeFound("birthDate.specified=true");

        // Get all the personalDetailsList where birthDate is null
        defaultPersonalDetailsShouldNotBeFound("birthDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate is greater than or equal to DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.greaterThanOrEqual=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate is greater than or equal to UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.greaterThanOrEqual=" + UPDATED_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate is less than or equal to DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.lessThanOrEqual=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate is less than or equal to SMALLER_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.lessThanOrEqual=" + SMALLER_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsLessThanSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate is less than DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.lessThan=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate is less than UPDATED_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.lessThan=" + UPDATED_BIRTH_DATE);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByBirthDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where birthDate is greater than DEFAULT_BIRTH_DATE
        defaultPersonalDetailsShouldNotBeFound("birthDate.greaterThan=" + DEFAULT_BIRTH_DATE);

        // Get all the personalDetailsList where birthDate is greater than SMALLER_BIRTH_DATE
        defaultPersonalDetailsShouldBeFound("birthDate.greaterThan=" + SMALLER_BIRTH_DATE);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email equals to DEFAULT_EMAIL
        defaultPersonalDetailsShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the personalDetailsList where email equals to UPDATED_EMAIL
        defaultPersonalDetailsShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email not equals to DEFAULT_EMAIL
        defaultPersonalDetailsShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the personalDetailsList where email not equals to UPDATED_EMAIL
        defaultPersonalDetailsShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultPersonalDetailsShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the personalDetailsList where email equals to UPDATED_EMAIL
        defaultPersonalDetailsShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email is not null
        defaultPersonalDetailsShouldBeFound("email.specified=true");

        // Get all the personalDetailsList where email is null
        defaultPersonalDetailsShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonalDetailsByEmailContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email contains DEFAULT_EMAIL
        defaultPersonalDetailsShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the personalDetailsList where email contains UPDATED_EMAIL
        defaultPersonalDetailsShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where email does not contain DEFAULT_EMAIL
        defaultPersonalDetailsShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the personalDetailsList where email does not contain UPDATED_EMAIL
        defaultPersonalDetailsShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllPersonalDetailsByAdressIsEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress equals to DEFAULT_ADRESS
        defaultPersonalDetailsShouldBeFound("adress.equals=" + DEFAULT_ADRESS);

        // Get all the personalDetailsList where adress equals to UPDATED_ADRESS
        defaultPersonalDetailsShouldNotBeFound("adress.equals=" + UPDATED_ADRESS);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByAdressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress not equals to DEFAULT_ADRESS
        defaultPersonalDetailsShouldNotBeFound("adress.notEquals=" + DEFAULT_ADRESS);

        // Get all the personalDetailsList where adress not equals to UPDATED_ADRESS
        defaultPersonalDetailsShouldBeFound("adress.notEquals=" + UPDATED_ADRESS);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByAdressIsInShouldWork() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress in DEFAULT_ADRESS or UPDATED_ADRESS
        defaultPersonalDetailsShouldBeFound("adress.in=" + DEFAULT_ADRESS + "," + UPDATED_ADRESS);

        // Get all the personalDetailsList where adress equals to UPDATED_ADRESS
        defaultPersonalDetailsShouldNotBeFound("adress.in=" + UPDATED_ADRESS);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByAdressIsNullOrNotNull() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress is not null
        defaultPersonalDetailsShouldBeFound("adress.specified=true");

        // Get all the personalDetailsList where adress is null
        defaultPersonalDetailsShouldNotBeFound("adress.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonalDetailsByAdressContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress contains DEFAULT_ADRESS
        defaultPersonalDetailsShouldBeFound("adress.contains=" + DEFAULT_ADRESS);

        // Get all the personalDetailsList where adress contains UPDATED_ADRESS
        defaultPersonalDetailsShouldNotBeFound("adress.contains=" + UPDATED_ADRESS);
    }

    @Test
    @Transactional
    public void getAllPersonalDetailsByAdressNotContainsSomething() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        // Get all the personalDetailsList where adress does not contain DEFAULT_ADRESS
        defaultPersonalDetailsShouldNotBeFound("adress.doesNotContain=" + DEFAULT_ADRESS);

        // Get all the personalDetailsList where adress does not contain UPDATED_ADRESS
        defaultPersonalDetailsShouldBeFound("adress.doesNotContain=" + UPDATED_ADRESS);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPersonalDetailsShouldBeFound(String filter) throws Exception {
        restPersonalDetailsMockMvc.perform(get("/api/personal-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personalDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].nationalId").value(hasItem(DEFAULT_NATIONAL_ID)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(DEFAULT_BIRTH_DATE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS)));

        // Check, that the count call also returns 1
        restPersonalDetailsMockMvc.perform(get("/api/personal-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPersonalDetailsShouldNotBeFound(String filter) throws Exception {
        restPersonalDetailsMockMvc.perform(get("/api/personal-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPersonalDetailsMockMvc.perform(get("/api/personal-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPersonalDetails() throws Exception {
        // Get the personalDetails
        restPersonalDetailsMockMvc.perform(get("/api/personal-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonalDetails() throws Exception {
        // Initialize the database
        personalDetailsRepository.saveAndFlush(personalDetails);

        int databaseSizeBeforeUpdate = personalDetailsRepository.findAll().size();

        // Update the personalDetails
        PersonalDetails updatedPersonalDetails = personalDetailsRepository.findById(personalDetails.getId()).get();
        // Disconnect from session so that the updates on updatedPersonalDetails are not directly saved in db
        em.detach(updatedPersonalDetails);
        updatedPersonalDetails
            .fullName(UPDATED_FULL_NAME)
            .mobileNo(UPDATED_MOBILE_NO)
            .nationalId(UPDATED_NATIONAL_ID)
            .birthDate(UPDATED_BIRTH_DATE)
            .email(UPDATED_EMAIL)
            .adress(UPDATED_ADRESS);
        PersonalDetailsDTO personalDetailsDTO = personalDetailsMapper.toDto(updatedPersonalDetails);

        restPersonalDetailsMockMvc.perform(put("/api/personal-details")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personalDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the PersonalDetails in the database
        List<PersonalDetails> personalDetailsList = personalDetailsRepository.findAll();
        assertThat(personalDetailsList).hasSize(databaseSizeBeforeUpdate);
        PersonalDetails testPersonalDetails = personalDetailsList.get(personalDetailsList.size() - 1);
        assertThat(testPersonalDetails.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testPersonalDetails.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testPersonalDetails.getNationalId()).isEqualTo(UPDATED_NATIONAL_ID);
        assertThat(testPersonalDetails.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
        assertThat(testPersonalDetails.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPersonalDetails.getAdress()).isEqualTo(UPDATED_ADRESS);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonalDetails() throws Exception {
        int databaseSizeBeforeUpdate = personalDetailsRepository.findAll().size();

        // Create the PersonalDetails
        PersonalDetailsDTO personalDetailsDTO = personalDetailsMapper.toDto(personalDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonalDetailsMockMvc.perform(put("/api/personal-details")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personalDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonalDetails in the database
        List<PersonalDetails> personalDetailsList = personalDetailsRepository.findAll();
        assertThat(personalDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
//    public void deletePersonalDetails() throws Exception {
//        // Initialize the database
//        personalDetailsRepository.saveAndFlush(personalDetails);
//
//        int databaseSizeBeforeDelete = personalDetailsRepository.findAll().size();
//
//        // Delete the personalDetails
//        restPersonalDetailsMockMvc.perform(delete("/api/personal-details/{id}", personalDetails.getId())
////            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<PersonalDetails> personalDetailsList = personalDetailsRepository.findAll();
//        assertThat(personalDetailsList).hasSize(databaseSizeBeforeDelete - 1);
//    }
}
