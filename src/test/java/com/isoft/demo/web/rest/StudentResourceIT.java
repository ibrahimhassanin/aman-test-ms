package com.isoft.demo.web.rest;

import com.isoft.demo.DemoApp;
import com.isoft.demo.config.SecurityBeanOverrideConfiguration;
import com.isoft.demo.domain.Student;
import com.isoft.demo.domain.PersonalDetails;
import com.isoft.demo.repository.StudentRepository;
import com.isoft.demo.service.StudentService;
import com.isoft.demo.service.dto.StudentDTO;
import com.isoft.demo.service.mapper.StudentMapper;
import com.isoft.demo.web.rest.errors.ExceptionTranslator;
import com.isoft.demo.service.dto.StudentCriteria;
import com.isoft.demo.service.StudentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.isoft.demo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StudentResource} REST controller.
 */
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, DemoApp.class})
public class StudentResourceIT {

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final Integer DEFAULT_DEPARTMENT = 1;
    private static final Integer UPDATED_DEPARTMENT = 2;
    private static final Integer SMALLER_DEPARTMENT = 1 - 1;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;
    private static final Integer SMALLER_STATUS = 1 - 1;

    private static final String DEFAULT_STUDENT_CRITERIA = "AAAAAAAAAA";
    private static final String UPDATED_STUDENT_CRITERIA = "BBBBBBBBBB";

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentQueryService studentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restStudentMockMvc;

    private Student student;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StudentResource studentResource = new StudentResource(studentService, studentQueryService);
        this.restStudentMockMvc = MockMvcBuilders.standaloneSetup(studentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Student createEntity(EntityManager em) {
        Student student = new Student()
            .level(DEFAULT_LEVEL)
            .department(DEFAULT_DEPARTMENT)
            .code(DEFAULT_CODE)
            .status(DEFAULT_STATUS)
            .studentCriteria(DEFAULT_STUDENT_CRITERIA);
        return student;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Student createUpdatedEntity(EntityManager em) {
        Student student = new Student()
            .level(UPDATED_LEVEL)
            .department(UPDATED_DEPARTMENT)
            .code(UPDATED_CODE)
            .status(UPDATED_STATUS)
            .studentCriteria(UPDATED_STUDENT_CRITERIA);
        return student;
    }

    @BeforeEach
    public void initTest() {
        student = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudent() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        // Create the Student
        StudentDTO studentDTO = studentMapper.toDto(student);
        restStudentMockMvc.perform(post("/api/students")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isCreated());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate + 1);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testStudent.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testStudent.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testStudent.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testStudent.getStudentCriteria()).isEqualTo(DEFAULT_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void createStudentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        // Create the Student with an existing ID
        student.setId(1L);
        StudentDTO studentDTO = studentMapper.toDto(student);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentMockMvc.perform(post("/api/students")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllStudents() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList
        restStudentMockMvc.perform(get("/api/students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId().intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].studentCriteria").value(hasItem(DEFAULT_STUDENT_CRITERIA)));
    }
    
    @Test
    @Transactional
    public void getStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", student.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(student.getId().intValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.studentCriteria").value(DEFAULT_STUDENT_CRITERIA));
    }


    @Test
    @Transactional
    public void getStudentsByIdFiltering() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        Long id = student.getId();

        defaultStudentShouldBeFound("id.equals=" + id);
        defaultStudentShouldNotBeFound("id.notEquals=" + id);

        defaultStudentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultStudentShouldNotBeFound("id.greaterThan=" + id);

        defaultStudentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultStudentShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllStudentsByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level equals to DEFAULT_LEVEL
        defaultStudentShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the studentList where level equals to UPDATED_LEVEL
        defaultStudentShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level not equals to DEFAULT_LEVEL
        defaultStudentShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the studentList where level not equals to UPDATED_LEVEL
        defaultStudentShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultStudentShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the studentList where level equals to UPDATED_LEVEL
        defaultStudentShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level is not null
        defaultStudentShouldBeFound("level.specified=true");

        // Get all the studentList where level is null
        defaultStudentShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level is greater than or equal to DEFAULT_LEVEL
        defaultStudentShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the studentList where level is greater than or equal to UPDATED_LEVEL
        defaultStudentShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level is less than or equal to DEFAULT_LEVEL
        defaultStudentShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the studentList where level is less than or equal to SMALLER_LEVEL
        defaultStudentShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level is less than DEFAULT_LEVEL
        defaultStudentShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the studentList where level is less than UPDATED_LEVEL
        defaultStudentShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllStudentsByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where level is greater than DEFAULT_LEVEL
        defaultStudentShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the studentList where level is greater than SMALLER_LEVEL
        defaultStudentShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }


    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department equals to DEFAULT_DEPARTMENT
        defaultStudentShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department equals to UPDATED_DEPARTMENT
        defaultStudentShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department not equals to DEFAULT_DEPARTMENT
        defaultStudentShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department not equals to UPDATED_DEPARTMENT
        defaultStudentShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultStudentShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the studentList where department equals to UPDATED_DEPARTMENT
        defaultStudentShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department is not null
        defaultStudentShouldBeFound("department.specified=true");

        // Get all the studentList where department is null
        defaultStudentShouldNotBeFound("department.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department is greater than or equal to DEFAULT_DEPARTMENT
        defaultStudentShouldBeFound("department.greaterThanOrEqual=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department is greater than or equal to UPDATED_DEPARTMENT
        defaultStudentShouldNotBeFound("department.greaterThanOrEqual=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department is less than or equal to DEFAULT_DEPARTMENT
        defaultStudentShouldBeFound("department.lessThanOrEqual=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department is less than or equal to SMALLER_DEPARTMENT
        defaultStudentShouldNotBeFound("department.lessThanOrEqual=" + SMALLER_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsLessThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department is less than DEFAULT_DEPARTMENT
        defaultStudentShouldNotBeFound("department.lessThan=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department is less than UPDATED_DEPARTMENT
        defaultStudentShouldBeFound("department.lessThan=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllStudentsByDepartmentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where department is greater than DEFAULT_DEPARTMENT
        defaultStudentShouldNotBeFound("department.greaterThan=" + DEFAULT_DEPARTMENT);

        // Get all the studentList where department is greater than SMALLER_DEPARTMENT
        defaultStudentShouldBeFound("department.greaterThan=" + SMALLER_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllStudentsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code equals to DEFAULT_CODE
        defaultStudentShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the studentList where code equals to UPDATED_CODE
        defaultStudentShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllStudentsByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code not equals to DEFAULT_CODE
        defaultStudentShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the studentList where code not equals to UPDATED_CODE
        defaultStudentShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllStudentsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code in DEFAULT_CODE or UPDATED_CODE
        defaultStudentShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the studentList where code equals to UPDATED_CODE
        defaultStudentShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllStudentsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code is not null
        defaultStudentShouldBeFound("code.specified=true");

        // Get all the studentList where code is null
        defaultStudentShouldNotBeFound("code.specified=false");
    }
                @Test
    @Transactional
    public void getAllStudentsByCodeContainsSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code contains DEFAULT_CODE
        defaultStudentShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the studentList where code contains UPDATED_CODE
        defaultStudentShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllStudentsByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where code does not contain DEFAULT_CODE
        defaultStudentShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the studentList where code does not contain UPDATED_CODE
        defaultStudentShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }


    @Test
    @Transactional
    public void getAllStudentsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status equals to DEFAULT_STATUS
        defaultStudentShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the studentList where status equals to UPDATED_STATUS
        defaultStudentShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status not equals to DEFAULT_STATUS
        defaultStudentShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the studentList where status not equals to UPDATED_STATUS
        defaultStudentShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultStudentShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the studentList where status equals to UPDATED_STATUS
        defaultStudentShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status is not null
        defaultStudentShouldBeFound("status.specified=true");

        // Get all the studentList where status is null
        defaultStudentShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status is greater than or equal to DEFAULT_STATUS
        defaultStudentShouldBeFound("status.greaterThanOrEqual=" + DEFAULT_STATUS);

        // Get all the studentList where status is greater than or equal to UPDATED_STATUS
        defaultStudentShouldNotBeFound("status.greaterThanOrEqual=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status is less than or equal to DEFAULT_STATUS
        defaultStudentShouldBeFound("status.lessThanOrEqual=" + DEFAULT_STATUS);

        // Get all the studentList where status is less than or equal to SMALLER_STATUS
        defaultStudentShouldNotBeFound("status.lessThanOrEqual=" + SMALLER_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status is less than DEFAULT_STATUS
        defaultStudentShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the studentList where status is less than UPDATED_STATUS
        defaultStudentShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllStudentsByStatusIsGreaterThanSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where status is greater than DEFAULT_STATUS
        defaultStudentShouldNotBeFound("status.greaterThan=" + DEFAULT_STATUS);

        // Get all the studentList where status is greater than SMALLER_STATUS
        defaultStudentShouldBeFound("status.greaterThan=" + SMALLER_STATUS);
    }


    @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria equals to DEFAULT_STUDENT_CRITERIA
        defaultStudentShouldBeFound("studentCriteria.equals=" + DEFAULT_STUDENT_CRITERIA);

        // Get all the studentList where studentCriteria equals to UPDATED_STUDENT_CRITERIA
        defaultStudentShouldNotBeFound("studentCriteria.equals=" + UPDATED_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria not equals to DEFAULT_STUDENT_CRITERIA
        defaultStudentShouldNotBeFound("studentCriteria.notEquals=" + DEFAULT_STUDENT_CRITERIA);

        // Get all the studentList where studentCriteria not equals to UPDATED_STUDENT_CRITERIA
        defaultStudentShouldBeFound("studentCriteria.notEquals=" + UPDATED_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaIsInShouldWork() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria in DEFAULT_STUDENT_CRITERIA or UPDATED_STUDENT_CRITERIA
        defaultStudentShouldBeFound("studentCriteria.in=" + DEFAULT_STUDENT_CRITERIA + "," + UPDATED_STUDENT_CRITERIA);

        // Get all the studentList where studentCriteria equals to UPDATED_STUDENT_CRITERIA
        defaultStudentShouldNotBeFound("studentCriteria.in=" + UPDATED_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaIsNullOrNotNull() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria is not null
        defaultStudentShouldBeFound("studentCriteria.specified=true");

        // Get all the studentList where studentCriteria is null
        defaultStudentShouldNotBeFound("studentCriteria.specified=false");
    }
                @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaContainsSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria contains DEFAULT_STUDENT_CRITERIA
        defaultStudentShouldBeFound("studentCriteria.contains=" + DEFAULT_STUDENT_CRITERIA);

        // Get all the studentList where studentCriteria contains UPDATED_STUDENT_CRITERIA
        defaultStudentShouldNotBeFound("studentCriteria.contains=" + UPDATED_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void getAllStudentsByStudentCriteriaNotContainsSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get all the studentList where studentCriteria does not contain DEFAULT_STUDENT_CRITERIA
        defaultStudentShouldNotBeFound("studentCriteria.doesNotContain=" + DEFAULT_STUDENT_CRITERIA);

        // Get all the studentList where studentCriteria does not contain UPDATED_STUDENT_CRITERIA
        defaultStudentShouldBeFound("studentCriteria.doesNotContain=" + UPDATED_STUDENT_CRITERIA);
    }


    @Test
    @Transactional
    public void getAllStudentsByPersonalDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);
        PersonalDetails personalDetails = PersonalDetailsResourceIT.createEntity(em);
        em.persist(personalDetails);
        em.flush();
        student.setPersonalDetails(personalDetails);
        studentRepository.saveAndFlush(student);
        Long personalDetailsId = personalDetails.getId();

        // Get all the studentList where personalDetails equals to personalDetailsId
        defaultStudentShouldBeFound("personalDetailsId.equals=" + personalDetailsId);

        // Get all the studentList where personalDetails equals to personalDetailsId + 1
        defaultStudentShouldNotBeFound("personalDetailsId.equals=" + (personalDetailsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultStudentShouldBeFound(String filter) throws Exception {
        restStudentMockMvc.perform(get("/api/students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId().intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].studentCriteria").value(hasItem(DEFAULT_STUDENT_CRITERIA)));

        // Check, that the count call also returns 1
        restStudentMockMvc.perform(get("/api/students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultStudentShouldNotBeFound(String filter) throws Exception {
        restStudentMockMvc.perform(get("/api/students?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restStudentMockMvc.perform(get("/api/students/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingStudent() throws Exception {
        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Update the student
        Student updatedStudent = studentRepository.findById(student.getId()).get();
        // Disconnect from session so that the updates on updatedStudent are not directly saved in db
        em.detach(updatedStudent);
        updatedStudent
            .level(UPDATED_LEVEL)
            .department(UPDATED_DEPARTMENT)
            .code(UPDATED_CODE)
            .status(UPDATED_STATUS)
            .studentCriteria(UPDATED_STUDENT_CRITERIA);
        StudentDTO studentDTO = studentMapper.toDto(updatedStudent);

        restStudentMockMvc.perform(put("/api/students")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isOk());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testStudent.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testStudent.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testStudent.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testStudent.getStudentCriteria()).isEqualTo(UPDATED_STUDENT_CRITERIA);
    }

    @Test
    @Transactional
    public void updateNonExistingStudent() throws Exception {
        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Create the Student
        StudentDTO studentDTO = studentMapper.toDto(student);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentMockMvc.perform(put("/api/students")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate);
    }
//
//    @Test
//    @Transactional
//    public void deleteStudent() throws Exception {
//        // Initialize the database
//        studentRepository.saveAndFlush(student);
//
//        int databaseSizeBeforeDelete = studentRepository.findAll().size();
//
//        // Delete the student
//        restStudentMockMvc.perform(delete("/api/students/{id}", student.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Student> studentList = studentRepository.findAll();
//        assertThat(studentList).hasSize(databaseSizeBeforeDelete - 1);
//    }
}
