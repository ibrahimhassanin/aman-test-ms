package com.isoft.demo.domain;

import com.isoft.demo.service.dto.AJsonType;
import com.isoft.demo.service.dto.AddressJsonType;
import com.isoft.demo.service.dto.BJsonType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "level")
    private Integer level;

    @Column(name = "department")
    private Integer department;

    @Column(name = "code")
    private String code;

    @Column(name = "status")
    private Integer status;

    @Type(type = "jsonb")
    @Column(name = "address_json_type", columnDefinition = "jsonb")
    private AddressJsonType addressJsonType;

    @Type(type = "jsonb")
    @Column(name = "a_json_type", columnDefinition = "jsonb")
    private AJsonType aJsonType;

    @Type(type = "jsonb")
    @Column(name = "b_json_type", columnDefinition = "jsonb")
    private BJsonType bJsonType;

    @Type(type = "jsonb")
    @Column(name = "student_criteria", columnDefinition = "jsonb")
    private String studentCriteria;

    @OneToOne
    @JoinColumn(unique = true)
    private PersonalDetails personalDetails;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public Student level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getDepartment() {
        return department;
    }

    public Student department(Integer department) {
        this.department = department;
        return this;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getCode() {
        return code;
    }

    public Student code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public Student status(Integer status) {
        this.status = status;
        return this;
    }

    public AddressJsonType getAddressJsonType() {
        return addressJsonType;
    }

    public void setAddressJsonType(AddressJsonType addressJsonType) {
        this.addressJsonType = addressJsonType;
    }

    public Student addressJsonType(AddressJsonType addressJsonType) {
        this.addressJsonType = addressJsonType;
        return this;
    }


    public Student aJsonType(AJsonType aJsonType) {
        this.aJsonType = aJsonType;
        return this;
    }

    public Student bJsonType(BJsonType bJsonType) {
        this.bJsonType = bJsonType;
        return this;
    }

    public AJsonType getaJsonType() {
        return aJsonType;
    }

    public void setaJsonType(AJsonType aJsonType) {
        this.aJsonType = aJsonType;
    }

    public BJsonType getbJsonType() {
        return bJsonType;
    }

    public void setbJsonType(BJsonType bJsonType) {
        this.bJsonType = bJsonType;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStudentCriteria() {
        return studentCriteria;
    }

    public Student studentCriteria(String studentCriteria) {
        this.studentCriteria = studentCriteria;
        return this;
    }

    public void setStudentCriteria(String studentCriteria) {
        this.studentCriteria = studentCriteria;
    }

    public PersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    public Student personalDetails(PersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
        return this;
    }

    public void setPersonalDetails(PersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        return id != null && id.equals(((Student) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + id +
            ", level=" + level +
            ", department=" + department +
            ", code='" + code + '\'' +
            ", status=" + status +
            ", addressJsonType=" + addressJsonType +
            ", aJsonType=" + aJsonType +
            ", bJsonType=" + bJsonType +
            ", studentCriteria='" + studentCriteria + '\'' +
            ", personalDetails=" + personalDetails +
            '}';
    }
}
