package com.isoft.demo.service;

import com.isoft.demo.service.dto.PersonalDetailsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.isoft.demo.domain.PersonalDetails}.
 */
public interface PersonalDetailsService {

    /**
     * Save a personalDetails.
     *
     * @param personalDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    PersonalDetailsDTO save(PersonalDetailsDTO personalDetailsDTO);

    /**
     * Get all the personalDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PersonalDetailsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" personalDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PersonalDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" personalDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
