package com.isoft.demo.service.mapper;

import com.isoft.demo.domain.*;
import com.isoft.demo.service.dto.StudentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Student} and its DTO {@link StudentDTO}.
 */
@Mapper(componentModel = "spring", uses = {PersonalDetailsMapper.class})
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {

    @Mapping(source = "personalDetails.id", target = "personalDetailsId")
    StudentDTO toDto(Student student);

    @Mapping(source = "personalDetailsId", target = "personalDetails")
    Student toEntity(StudentDTO studentDTO);

    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }
}
