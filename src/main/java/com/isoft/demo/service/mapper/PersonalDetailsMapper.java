package com.isoft.demo.service.mapper;

import com.isoft.demo.domain.*;
import com.isoft.demo.service.dto.PersonalDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PersonalDetails} and its DTO {@link PersonalDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonalDetailsMapper extends EntityMapper<PersonalDetailsDTO, PersonalDetails> {



    default PersonalDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonalDetails personalDetails = new PersonalDetails();
        personalDetails.setId(id);
        return personalDetails;
    }
}
