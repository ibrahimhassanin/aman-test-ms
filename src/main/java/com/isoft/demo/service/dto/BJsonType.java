package com.isoft.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BJsonType implements Serializable {

    @JsonProperty("b1")
    private String b1;

    @JsonProperty("b2")
    private String b2;


    public BJsonType() {
    }


    public String getB1() {
        return b1;
    }

    public void setB1(String b1) {
        this.b1 = b1;
    }

    public String getB2() {
        return b2;
    }

    public void setB2(String b2) {
        this.b2 = b2;
    }

    @Override
    public String toString() {
        return "BJsonType{" +
            "b1='" + b1 + '\'' +
            ", b2='" + b2 + '\'' +
            '}';
    }
}
