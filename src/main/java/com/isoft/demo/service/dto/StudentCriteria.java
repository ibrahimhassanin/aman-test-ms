package com.isoft.demo.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.isoft.demo.domain.Student} entity. This class is used
 * in {@link com.isoft.demo.web.rest.StudentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /students?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StudentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter level;

    private IntegerFilter department;

    private StringFilter code;

    private IntegerFilter status;

    private StringFilter studentCriteria;

    private LongFilter personalDetailsId;

    public StudentCriteria(){
    }

    public StudentCriteria(StudentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.level = other.level == null ? null : other.level.copy();
        this.department = other.department == null ? null : other.department.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.studentCriteria = other.studentCriteria == null ? null : other.studentCriteria.copy();
        this.personalDetailsId = other.personalDetailsId == null ? null : other.personalDetailsId.copy();
    }

    @Override
    public StudentCriteria copy() {
        return new StudentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getLevel() {
        return level;
    }

    public void setLevel(IntegerFilter level) {
        this.level = level;
    }

    public IntegerFilter getDepartment() {
        return department;
    }

    public void setDepartment(IntegerFilter department) {
        this.department = department;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getStudentCriteria() {
        return studentCriteria;
    }

    public void setStudentCriteria(StringFilter studentCriteria) {
        this.studentCriteria = studentCriteria;
    }

    public LongFilter getPersonalDetailsId() {
        return personalDetailsId;
    }

    public void setPersonalDetailsId(LongFilter personalDetailsId) {
        this.personalDetailsId = personalDetailsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StudentCriteria that = (StudentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(level, that.level) &&
            Objects.equals(department, that.department) &&
            Objects.equals(code, that.code) &&
            Objects.equals(status, that.status) &&
            Objects.equals(studentCriteria, that.studentCriteria) &&
            Objects.equals(personalDetailsId, that.personalDetailsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        level,
        department,
        code,
        status,
        studentCriteria,
        personalDetailsId
        );
    }

    @Override
    public String toString() {
        return "StudentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (level != null ? "level=" + level + ", " : "") +
                (department != null ? "department=" + department + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (studentCriteria != null ? "studentCriteria=" + studentCriteria + ", " : "") +
                (personalDetailsId != null ? "personalDetailsId=" + personalDetailsId + ", " : "") +
            "}";
    }

}
