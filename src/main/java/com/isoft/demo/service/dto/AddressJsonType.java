package com.isoft.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AddressJsonType implements Serializable {

    @JsonProperty("country")
    private String country;

    @JsonProperty("city")
    private String city;

    @JsonProperty("zipCode")
    private Long zipCode;

    @JsonProperty("compositeAddressList")
    private List<CompositeAddress> compositeAddressList;

    public AddressJsonType() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<CompositeAddress> getCompositeAddressList() {
        return compositeAddressList;
    }

    public void setCompositeAddressList(List<CompositeAddress> compositeAddressList) {
        this.compositeAddressList = compositeAddressList;
    }

    public Long getZipCode() {
        return zipCode;
    }

    public void setZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "AddressJsonType{" +
            "country='" + country + '\'' +
            ", city='" + city + '\'' +
            ", zipCode=" + zipCode +
            '}';
    }
}
