package com.isoft.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AJsonType implements Serializable {

    @JsonProperty("a1")
    private String a1;

    @JsonProperty("a2")
    private String a2;

    public AJsonType() {
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    @Override
    public String toString() {
        return "AJsonType{" +
            "a1='" + a1 + '\'' +
            ", a2='" + a2 + '\'' +
            '}';
    }
}
