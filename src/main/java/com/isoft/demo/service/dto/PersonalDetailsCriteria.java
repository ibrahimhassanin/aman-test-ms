package com.isoft.demo.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.isoft.demo.domain.PersonalDetails} entity. This class is used
 * in {@link com.isoft.demo.web.rest.PersonalDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /personal-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PersonalDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter fullName;

    private StringFilter mobileNo;

    private StringFilter nationalId;

    private LocalDateFilter birthDate;

    private StringFilter email;

    private StringFilter adress;

    public PersonalDetailsCriteria(){
    }

    public PersonalDetailsCriteria(PersonalDetailsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.fullName = other.fullName == null ? null : other.fullName.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.nationalId = other.nationalId == null ? null : other.nationalId.copy();
        this.birthDate = other.birthDate == null ? null : other.birthDate.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.adress = other.adress == null ? null : other.adress.copy();
    }

    @Override
    public PersonalDetailsCriteria copy() {
        return new PersonalDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFullName() {
        return fullName;
    }

    public void setFullName(StringFilter fullName) {
        this.fullName = fullName;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public StringFilter getNationalId() {
        return nationalId;
    }

    public void setNationalId(StringFilter nationalId) {
        this.nationalId = nationalId;
    }

    public LocalDateFilter getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateFilter birthDate) {
        this.birthDate = birthDate;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getAdress() {
        return adress;
    }

    public void setAdress(StringFilter adress) {
        this.adress = adress;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PersonalDetailsCriteria that = (PersonalDetailsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(fullName, that.fullName) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(nationalId, that.nationalId) &&
            Objects.equals(birthDate, that.birthDate) &&
            Objects.equals(email, that.email) &&
            Objects.equals(adress, that.adress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        fullName,
        mobileNo,
        nationalId,
        birthDate,
        email,
        adress
        );
    }

    @Override
    public String toString() {
        return "PersonalDetailsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fullName != null ? "fullName=" + fullName + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (nationalId != null ? "nationalId=" + nationalId + ", " : "") +
                (birthDate != null ? "birthDate=" + birthDate + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (adress != null ? "adress=" + adress + ", " : "") +
            "}";
    }

}
