package com.isoft.demo.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.isoft.demo.domain.Student} entity.
 */
public class StudentDTO implements Serializable {

    private Long id;

    private Integer level;

    private Integer department;

    private String code;

    private Integer status;

    private String studentCriteria;

    private AddressJsonType addressJsonType;

    private Long personalDetailsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStudentCriteria() {
        return studentCriteria;
    }

    public void setStudentCriteria(String studentCriteria) {
        this.studentCriteria = studentCriteria;
    }

    public Long getPersonalDetailsId() {
        return personalDetailsId;
    }

    public void setPersonalDetailsId(Long personalDetailsId) {
        this.personalDetailsId = personalDetailsId;
    }

    public AddressJsonType getAddressJsonType() {
        return addressJsonType;
    }

    public void setAddressJsonType(AddressJsonType addressJsonType) {
        this.addressJsonType = addressJsonType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StudentDTO studentDTO = (StudentDTO) o;
        if (studentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), studentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StudentDTO{" +
            "id=" + getId() +
            ", level=" + getLevel() +
            ", department=" + getDepartment() +
            ", code='" + getCode() + "'" +
            ", status=" + getStatus() +
            ", studentCriteria='" + getStudentCriteria() + "'" +
            ", personalDetailsId=" + getPersonalDetailsId() +
            "}";
    }
}
