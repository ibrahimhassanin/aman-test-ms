package com.isoft.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CompositeAddress implements Serializable {

    @JsonProperty("descAr")
    private String descAr;

    @JsonProperty("descEn")
    private String descEn;

    public CompositeAddress() {
    }

    public CompositeAddress(String descAr, String descEn) {
        this.descAr = descAr;
        this.descEn = descEn;
    }

    public String getDescAr() {
        return descAr;
    }

    public void setDescAr(String descAr) {
        this.descAr = descAr;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    @Override
    public String toString() {
        return "CompositeAddress{" +
            "descAr='" + descAr + '\'' +
            ", descEn='" + descEn + '\'' +
            '}';
    }
}
