package com.isoft.demo.service.impl;

import com.isoft.demo.service.PersonalDetailsService;
import com.isoft.demo.domain.PersonalDetails;
import com.isoft.demo.repository.PersonalDetailsRepository;
import com.isoft.demo.service.dto.PersonalDetailsDTO;
import com.isoft.demo.service.mapper.PersonalDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PersonalDetails}.
 */
@Service
@Transactional
public class PersonalDetailsServiceImpl implements PersonalDetailsService {

    private final Logger log = LoggerFactory.getLogger(PersonalDetailsServiceImpl.class);

    private final PersonalDetailsRepository personalDetailsRepository;

    private final PersonalDetailsMapper personalDetailsMapper;

    public PersonalDetailsServiceImpl(PersonalDetailsRepository personalDetailsRepository, PersonalDetailsMapper personalDetailsMapper) {
        this.personalDetailsRepository = personalDetailsRepository;
        this.personalDetailsMapper = personalDetailsMapper;
    }

    /**
     * Save a personalDetails.
     *
     * @param personalDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PersonalDetailsDTO save(PersonalDetailsDTO personalDetailsDTO) {
        log.debug("Request to save PersonalDetails : {}", personalDetailsDTO);
        PersonalDetails personalDetails = personalDetailsMapper.toEntity(personalDetailsDTO);
        personalDetails = personalDetailsRepository.save(personalDetails);
        return personalDetailsMapper.toDto(personalDetails);
    }

    /**
     * Get all the personalDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonalDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonalDetails");
        return personalDetailsRepository.findAll(pageable)
            .map(personalDetailsMapper::toDto);
    }


    /**
     * Get one personalDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PersonalDetailsDTO> findOne(Long id) {
        log.debug("Request to get PersonalDetails : {}", id);
        return personalDetailsRepository.findById(id)
            .map(personalDetailsMapper::toDto);
    }

    /**
     * Delete the personalDetails by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonalDetails : {}", id);
        personalDetailsRepository.deleteById(id);
    }
}
