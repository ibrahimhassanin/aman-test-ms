package com.isoft.demo.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.isoft.demo.domain.PersonalDetails;
import com.isoft.demo.domain.*; // for static metamodels
import com.isoft.demo.repository.PersonalDetailsRepository;
import com.isoft.demo.service.dto.PersonalDetailsCriteria;
import com.isoft.demo.service.dto.PersonalDetailsDTO;
import com.isoft.demo.service.mapper.PersonalDetailsMapper;

/**
 * Service for executing complex queries for {@link PersonalDetails} entities in the database.
 * The main input is a {@link PersonalDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PersonalDetailsDTO} or a {@link Page} of {@link PersonalDetailsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PersonalDetailsQueryService extends QueryService<PersonalDetails> {

    private final Logger log = LoggerFactory.getLogger(PersonalDetailsQueryService.class);

    private final PersonalDetailsRepository personalDetailsRepository;

    private final PersonalDetailsMapper personalDetailsMapper;

    public PersonalDetailsQueryService(PersonalDetailsRepository personalDetailsRepository, PersonalDetailsMapper personalDetailsMapper) {
        this.personalDetailsRepository = personalDetailsRepository;
        this.personalDetailsMapper = personalDetailsMapper;
    }

    /**
     * Return a {@link List} of {@link PersonalDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PersonalDetailsDTO> findByCriteria(PersonalDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PersonalDetails> specification = createSpecification(criteria);
        return personalDetailsMapper.toDto(personalDetailsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PersonalDetailsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PersonalDetailsDTO> findByCriteria(PersonalDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PersonalDetails> specification = createSpecification(criteria);
        return personalDetailsRepository.findAll(specification, page)
            .map(personalDetailsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PersonalDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PersonalDetails> specification = createSpecification(criteria);
        return personalDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link PersonalDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PersonalDetails> createSpecification(PersonalDetailsCriteria criteria) {
        Specification<PersonalDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PersonalDetails_.id));
            }
            if (criteria.getFullName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFullName(), PersonalDetails_.fullName));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), PersonalDetails_.mobileNo));
            }
            if (criteria.getNationalId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNationalId(), PersonalDetails_.nationalId));
            }
            if (criteria.getBirthDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBirthDate(), PersonalDetails_.birthDate));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), PersonalDetails_.email));
            }
            if (criteria.getAdress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdress(), PersonalDetails_.adress));
            }
        }
        return specification;
    }
}
