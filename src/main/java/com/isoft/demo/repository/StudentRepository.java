package com.isoft.demo.repository;

import com.isoft.demo.domain.Student;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Student entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StudentRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor<Student> {

    Student findByCode(String code);

    @Query(value =
        "SELECT ST.*" +
            "  FROM STUDENT ST" +
            "  WHERE ST.ADDRESS_JSON_TYPE ->> 'city' = :city ", nativeQuery = true)
    List<Student> findByCityCriteria(@Param("city") String city);

    @Query(value =
        "SELECT ST.*" +
            "  FROM STUDENT ST" +
            "  WHERE ST.ADDRESS_JSON_TYPE ->> 'city' = :city " +
            "  AND ST.A_JSON_TYPE ->> 'a1' = :a1 ", nativeQuery = true)
    List<Student> findByCityAndA1(@Param("city") String city, @Param("a1") String a1);


    @Query(value =
        "SELECT ST.*" +
            "  FROM STUDENT ST" +
            "  WHERE ST.ADDRESS_JSON_TYPE.compositeAddressList ->> 'descEn' = :descEn ", nativeQuery = true)
    List<Student> findByListCondition(@Param("descEn") String descEn);
}

