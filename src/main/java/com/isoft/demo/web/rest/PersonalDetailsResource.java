package com.isoft.demo.web.rest;

import com.isoft.demo.service.PersonalDetailsService;
import com.isoft.demo.web.rest.errors.BadRequestAlertException;
import com.isoft.demo.service.dto.PersonalDetailsDTO;
import com.isoft.demo.service.dto.PersonalDetailsCriteria;
import com.isoft.demo.service.PersonalDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.isoft.demo.domain.PersonalDetails}.
 */
@RestController
@RequestMapping("/api")
public class PersonalDetailsResource {

    private final Logger log = LoggerFactory.getLogger(PersonalDetailsResource.class);

    private static final String ENTITY_NAME = "testJdlPersonalDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PersonalDetailsService personalDetailsService;

    private final PersonalDetailsQueryService personalDetailsQueryService;

    public PersonalDetailsResource(PersonalDetailsService personalDetailsService, PersonalDetailsQueryService personalDetailsQueryService) {
        this.personalDetailsService = personalDetailsService;
        this.personalDetailsQueryService = personalDetailsQueryService;
    }

    /**
     * {@code POST  /personal-details} : Create a new personalDetails.
     *
     * @param personalDetailsDTO the personalDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new personalDetailsDTO, or with status {@code 400 (Bad Request)} if the personalDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/personal-details")
    public ResponseEntity<PersonalDetailsDTO> createPersonalDetails(@RequestBody PersonalDetailsDTO personalDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save PersonalDetails : {}", personalDetailsDTO);
        if (personalDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new personalDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PersonalDetailsDTO result = personalDetailsService.save(personalDetailsDTO);
        return ResponseEntity.created(new URI("/api/personal-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /personal-details} : Updates an existing personalDetails.
     *
     * @param personalDetailsDTO the personalDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated personalDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the personalDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the personalDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/personal-details")
    public ResponseEntity<PersonalDetailsDTO> updatePersonalDetails(@RequestBody PersonalDetailsDTO personalDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update PersonalDetails : {}", personalDetailsDTO);
        if (personalDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PersonalDetailsDTO result = personalDetailsService.save(personalDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, personalDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /personal-details} : get all the personalDetails.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of personalDetails in body.
     */
    @GetMapping("/personal-details")
    public ResponseEntity<List<PersonalDetailsDTO>> getAllPersonalDetails(PersonalDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PersonalDetails by criteria: {}", criteria);
        Page<PersonalDetailsDTO> page = personalDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /personal-details/count} : count all the personalDetails.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/personal-details/count")
    public ResponseEntity<Long> countPersonalDetails(PersonalDetailsCriteria criteria) {
        log.debug("REST request to count PersonalDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(personalDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /personal-details/:id} : get the "id" personalDetails.
     *
     * @param id the id of the personalDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the personalDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/personal-details/{id}")
    public ResponseEntity<PersonalDetailsDTO> getPersonalDetails(@PathVariable Long id) {
        log.debug("REST request to get PersonalDetails : {}", id);
        Optional<PersonalDetailsDTO> personalDetailsDTO = personalDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personalDetailsDTO);
    }

    /**
     * {@code DELETE  /personal-details/:id} : delete the "id" personalDetails.
     *
     * @param id the id of the personalDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/personal-details/{id}")
    public ResponseEntity<Void> deletePersonalDetails(@PathVariable Long id) {
        log.debug("REST request to delete PersonalDetails : {}", id);
        personalDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
